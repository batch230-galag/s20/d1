// console.log("Hello World")

// [SECTION] While Loop
/*
    - A while loop takes in an expression/condition
    - Expressions are any unit of code that can be evaluated to a value
    - If the condition evaluates to true, the statements inside the code block will be executed
    - A statement is a command that the programmer gives to the computer
    - A loop will iterate a certain number of times until an expression/condition is met
    - "Iteration" is the term given to the repetition of statements
    - Syntax
        while(expression/condition) {
            statement
        }
*/

function divider(section) {
        console.log(`------- ${section} -------`)
    };

divider("While Loop");

    let count = 5;
    // initial value of count is 5

    while (count != 0) { // as long the condition is satisfied the loop code will still run
        console.log("While: " + count);
        count--;
    };

// [SECTION] Do While Loop
divider("Do While Loop");

    // let number = Number(prompt("Give me a number"));
    let number = 1;
    do {
        console.log("Do While: " + number);
        number += 1;
    } while (number < 10);

// [SECTION] For Loop
divider("For Loop")
/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

    
    for(let count = 0; count <= 20; count++) {
        // The current value of count is printed out
        console.log(count);
    }


    let myString = 'alex';
    for (let index = 0; index < myString.length; index++) {
        console.log(myString[index])
    }

    console.log(myString[0]);
    console.log(myString[1]);
    console.log(myString[2]);
    console.log(myString[3]);

    console.log("Lenght of myString: " + myString.length);

    let myName = 'AlEx';

/*
    - Creates a loop that will print out the letters of the name individually and print out 'vowels detected' instead when the letter to be printed out is a vowel
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "i"
        2. It will check if "i" is less than the length of myName (e.g. 0)
        3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
        4. If the expression/condition is true the console will print the number 3.
        5. If the letter is not a vowel the console will print the letter
        6. The value of "i" will be incremented by 1 (e.g. i = 1)
        7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
*/
    
    for (let i = 0; i < myName.length; i++) {
        // console.log(myName[i].toLowerCase());

        // If the character of your name is a vowel letter, instead of displaying the character, display number "3"
        // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match

        if (myName[i].toLowerCase() == 'a' || 
        myName[i].toLowerCase() == 'e' || 
        myName[i].toLowerCase() == 'i' || 
        myName[i].toLowerCase() == 'o' || 
        myName[i].toLowerCase() == 'u') {

            console.log('vowel detected');
        } else {
            console.log(myName[i]);
        }
    }



// [SECTION] Continue and Break
divider("Continue and Break");

    for (let count = 0; count <= 20; count++) {
        if(count % 2 === 0) {
            // Tells the code to continue to the next iteration of the loop
        // This ignores all statements located after the continue statement and go back with the loop condition to re-iterate;
            continue
        }
        console.log("Continue: " + count);

        if(count > 10) { // if the condition is satisfied the loop will stop
            console.log("Break: " + count);
            break;
        }
    }

    for (let count = 0; count <= 10; count++) {
        if (count == 5) {
            continue
        }
        console.log(count);

        if (count > 7) {
            break;
        }
    }


    name1 = "alexandro"
    for (let i=0; i <name1.length; i++) {
        console.log(name1[i]);

        if(name1[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration");
        continue;
        }

        if (name1[i] === "d") {
            break;
        }
    }



